#include "blocks.h"

// piece.cpp: implementation of the piece class.
//
//////////////////////////////////////////////////////////////////////

piece::piece()
{

	this->setStatus(FREE);
	this->icon = 0;
	

}

piece::~piece()
{
view = NULL;
}


int piece::getStatus()
{
	return this->status;
}

void piece::setStatus(int novo)
{

	status = novo;

}

void piece::setTabuleiro(tabuleiro *tab)
{

	this->view = tab;

}


tabuleiro * piece::getTabuleiro()
{

	return this->view;
}



void piece::place(int ax, int ay)
{

if(getTabuleiro()->isCircular() == false)
{

	if(ax<0) 
//		if(getTabuleiro()->isCircular())
//			ax = DEFSIZEX-1;
//		else 
		ax=0;

	if(ax>=DEFSIZEX) 
//		if(this->getTabuleiro()->isCircular())
//			ax = 0;
//		else 
		ax=DEFSIZEX-1;

	if(ay<0) 
//		if(getTabuleiro()->isCircular())
//			ay=DEFSIZEY-1;
//		else 
		ay=0;
	
	if(ay>=DEFSIZEY) 
//		if(getTabuleiro()->isCircular())
//			ay = 0;
//		else 
		ay=DEFSIZEY-1;

	this->setX(ax);
	this->setY(ay);

}

else
{
	if(ax<0) 
			ax = DEFSIZEX-ax-1;

	if(ax>=DEFSIZEX) 
			ax = 0+(ax-DEFSIZEX);

	if(ay<0) 
			ay=DEFSIZEY-1-ay;
	
	if(ay>=DEFSIZEY) 
			ay = 0+(ay-DEFSIZEY);

	this->setX(ax);
	this->setY(ay);
}

	this->getTabuleiro()->place(this);


}




void piece::setIcon(int x)
{
	icon = x;
}

int piece::getIcon()
{

	return this->icon;
}


bool piece::move(int length) // move com angulo
{
	for(int i=0; i< length; i++)
	if(!move(cos(getFacing())*getX(), sin(getFacing())*getY())) return false;

return true;
}



void piece::setFacing(float ang)
{
facing = ang;
}


float piece::getFacing()
{
return facing;
}


bool piece::move(int quantX, int quantY)
{

	this->getTabuleiro()->remove(this);

	if(quantX<=0)
	{
//		if(this->getTabuleiro()->isCircular() == false)
//		{
			this->place(this->getX(), this->getY());
			return false;
//		}
//		else
//		{
//			this->place(DEFSIZEX-1, this->getY());
//			return true;
//		}
	}
	 
	if(quantX>=DEFSIZEX)
	{
//		if(this->getTabuleiro()->isCircular() == false)
//		{
			this->place(this->getX(), this->getY());
			return false;
//		}
//		else
//		{
//			this->place(0, this->getY());
//			return true;
//		}

	}
	

	if(quantY<=0)
	{
//		if(this->getTabuleiro()->isCircular() == false)
//		{
		this->place(this->getX(), this->getY());
		return false;
//		}
//		else
//		{
//		this->place(getX(), DEFSIZEY-1);
//		return true;
//		}

	}
	

	if(quantY>=DEFSIZEY)
	{
//		if(this->getTabuleiro()->isCircular() == false)
//		{
			this->place(this->getX(), this->getY());
			return false;
//		}
//		else
//		{
//			this->place(getX(), 0);
//			return true;
//		}
	
	}



	if(this->view->PosicaoOcupada(quantX, quantY)) 
		{
		this->place(this->getX(), this->getY());
		//this->setStatus(BLOQUEADO);
		return false;
		}
	else {
		this->place(quantX, quantY);
		//this->setStatus(OK);
		return true;
		}
	
}


int piece::getY()
{
	return this->y;
}


int piece::getX()
{
	return this->x;
}


void piece::setX(int xi)
{
	this->x = xi;
}


void piece::setY(int yi)
{
	this->y = yi;
}




// block.cpp: implementation of the block class.
//
//////////////////////////////////////////////////////////////////////



block::block()
{

	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY;j++) 
			{
			this->blocos[i][j].setStatus(FREE);
			this->blocos[i][j].setFacing(90);

			}
	comprimento = 0;
	altura = 0;

	situation = 0;

}


block::~block()
{

	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY;j++) 
			blocos[i][j].~piece();

}


void block::setPiece(int ax, int ay)
{


		if(ax<0) ax=0;
		if(ax>=SIZEX) ax=SIZEX-1;

		if(ay<0) ay=0;
		if(ay>SIZEX) ay=SIZEY-1;


	this->blocos[ax][ay].setStatus(OK);

}


int block::getStatus()
{
return this->situation;

}

int block::getLength()
{
	return this->comprimento;

}



void block::setIcon(int x)
{

	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++) 
			this->blocos[i][j].setIcon(x);


}


void block::setStatus(int x)
{
	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++) 
			if(blocos[i][j].getStatus() != FREE)
				this->blocos[i][j].setStatus(x);


			this->situation = x;
}




int block::getHeight()
{
	return this->altura;

}

void block::place(int ax, int ay)
{


		if(ax<0) ax=0;
		if(ax+getLength()>=DEFSIZEX) ax=DEFSIZEX-getLength();

		if(ay<0) ay=0;
		if(ay+getHeight()>=DEFSIZEY) ay=DEFSIZEY-getHeight();


	
	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++) 
		{
			if(this->blocos[i][j].getStatus() != FREE)
				//if(this->blocos[i][j].getTabuleiro()->PosicaoOcupada(ax+i, ay+j) == false)
				this->blocos[i][j].place(ax+i, ay+j);
			
/*				{
					for(int g=0; g<=i; g++)
						for(int h=0; h<=j; h++)
							this->blocos[g][h].getTabuleiro()->remove(&blocos[g][h]);

					return false;
				}*/
		}

	this->xpos = ax;
	this->ypos = ay;

//return true;
}



void block::setTabuleiro(tabuleiro *tab)
{

	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			this->blocos[i][j].setTabuleiro(tab);

}

/*
bool block::moveDown(int x)
{





}


*/

bool block::move(int ax, int ay)
{

int xold = xpos, yold = ypos;

this->remove();

	
		bool blocked = false;
/*
for(int i = 0; i<SIZEX; i++)
	for(int j=0; j<SIZEY; j++)
		if(blocked == false)
			if(this->blocos[i][j].getStatus() != FREE)
			{
				if(this->blocos[i][j].move(ax+i, ay+j));
				else blocked = true;
			}
*/

	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			if(blocked == false)
				if(this->blocos[i][j].getStatus() != FREE) 
				{
					if(this->blocos[i][j].getTabuleiro()->PosicaoOcupada(ax+i, ay+j) == true)
						blocked = true;
					else 
					{
			//			this->blocos[i][j].move(ax+i, ay+j);
//						blocked = false;
					}
					//if (this->blocos[i][j].getStatus() == BLOQUEADO)
					//	blocked = true;
				}
		


if(blocked == false)
{

	xpos = ax;
	ypos = ay;

	this->place(ax, ay);
}


if(blocked == true)
{

this->place(xold, yold);

	
		xpos = xold;
		ypos = yold;

		return false;
} // fim do if(blocked)


return true;

}



tabuleiro * block::getTabuleiro()
{

	return this->blocos[0][0].getTabuleiro();
}




void block::setSize()
{
int jant = 0;
int iant = 0;

		for(int i=0; i<SIZEX; i++)
			for(int j=0; j<SIZEY; j++)
				if(blocos[i][j].getStatus() != FREE)
				{
					if (i> iant)
					{
						comprimento = i+1;
						iant = i;

					}
			
					if (j > jant)
					{
						altura = j+1;
						jant = j;
					}
				}
				if (altura == 0) altura++;
				if (comprimento == 0) comprimento++;

}


int block::getX()
{

		return xpos;

}



int block::getY()
{


	return ypos;
/*
bool armazenado = false;

for (int i=0; i<SIZEX; i++)
	for(int j=0; j<SIZEY; j++)
		if(armazenado == false)
			if(this->blocos[i][j].getTabuleiro() != NULL)
			{
				yold = this->blocos[i][j].getY();
				armazenado = true;
			}

			return yold;
*/
}

void block::setPos(int x, int y)
{

	this->xpos = x;
	this->ypos = y;


	for(int j=0; j<SIZEX; j++)
		for(int g=0; g<SIZEY; g++)
		{
			this->blocos[j][g].setX(x+j);
			this->blocos[j][g].setY(y+g);
		}

}


void block::remove()
{

for (int i=0; i<SIZEX; i++)
	for (int j=0; j<SIZEY; j++)
			if(this->blocos[i][j].getStatus() != FREE)
				blocos[i][j].getTabuleiro()->remove(&blocos[i][j]);


}


void block::unsetPiece(int x, int y)
{

	this->blocos[x][y].setStatus(FREE);
	this->blocos[x][y].getTabuleiro()->remove(&blocos[x][y]);

}




// tabuleiro.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////

tabuleiro::tabuleiro(int x, int y)
{
	this->sizex = x;
	this->sizey = y;

	init_tab();

	//this->setCircular(false);
}


tabuleiro::~tabuleiro()
{

}


void tabuleiro::place(piece *pec)
{

	
//	if(tab[pec->getX()][pec->getY()] == FREE)
//	{
		tab[pec->getX()][pec->getY()] = pec->getStatus();
//		return true;
//	}

//	else return false;


}


void tabuleiro::init_tab()
{

	for (int i = 0; i<DEFSIZEX; i++)
		for (int j = 0; j<DEFSIZEY; j++)
			tab[i][j] = FREE; 

}



bool tabuleiro::isCircular()
{
	return circular;
}



void tabuleiro::setCircular(bool is)
{
circular = is;
}


void tabuleiro::draw(int xin, int yin)
{

//	init_tab();

	for (int i = yin; i<DEFSIZEY; i++) {
		for (int j = xin; j<DEFSIZEX; j++)
				cout<<this->tab[j][i];
		cout<<endl;
	}


/*	piece *temp;

	vector<piece *>::iterator seek;
	for(seek = eixo.begin(); seek !=eixo.end(); seek++)
		{
		temp = *seek;

		tab[temp->getX()][temp->getY()] = temp->Icon();

		}


	for (int i = yin; i<DEFSIZEY; i++) {
		for (int j = xin; j<DEFSIZEX; j++)
				cout<<this->tab[j][i];
		cout<<endl;
				}
			
*/

}


void tabuleiro::removeAt(int x, int y)
{
	tab[x][y] = FREE;
}

bool tabuleiro::PosicaoOcupada(int x, int y)
{

if(this->isCircular() == false)
{

	if(x>=DEFSIZEX) return true;
	if(y>=DEFSIZEY) return true;

	if(x<0) 
	{
			return true;
	}

	if(y<0) 
	{
			return true;
	}

	if(tab[x][y] == FREE) return false;

	else return true;

}
// else...
if(tab[x][y] == FREE) return false;
else return true;


}



void tabuleiro::remove(piece *pec)
{

	tab[pec->getX()][pec->getY()] = FREE;
/*	piece * temp;

	vector<piece *>::iterator seek;
	for(seek = eixo.begin(); seek !=eixo.end(); seek++)
		{
		temp = *seek;


		if(temp == pec)
//	if(temp->getX() == pec->getX())
//		if (temp->getY() == pec->getY())
//			if (temp->getStatus() == pec->getStatus())
						eixo.erase(seek);
		}
	
*/
}


// framedblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////

framedblock::framedblock()
{

	actual_facing = 0;

	
	this->atual = &this->quadro[actual_facing];

}



framedblock::~framedblock()
{
	atual = NULL;
	
	for(int i=0; i<FRAMES; i++)
		quadro[i].~block();


}


void framedblock::setTabuleiro(tabuleiro *tab)
{
	for(int i=0; i<FRAMES; i++)
			quadro[i].setTabuleiro(tab);
}


void framedblock::place(int x, int y)
{

	
	this->atual->place(x,y);
	{

	for (int i = 0; i<FRAMES; i++)
	   if(i != this->actual_facing)
			{
				quadro[i].setPos(x, y);
			}
//	return true;
	}

//	return false;


}

bool framedblock::move(int x, int y)
{

	if(this->atual->move(x,y))
	{
	
	for (int i = 0; i<FRAMES; i++)
	   if(i != this->actual_facing)
			quadro[i].setPos(x, y);
	
	return true;
	}

	return false;
}


int framedblock::getX()
{
	return this->atual->getX();
}


int framedblock::getY()
{
	return this->atual->getY();
}



bool framedblock::rotate()
{


bool blocked = false; // a peca pode girar

atual->remove();


int pos=actual_facing+1;

	if(pos>FRAMES-1) pos = 0;
	if(pos<0) pos = FRAMES-1;


int xp = quadro[pos].getX();
int yp = quadro[pos].getY();

		if(xp<0) xp=0;
		if(xp+quadro[pos].getLength()>=DEFSIZEX) xp=DEFSIZEX-quadro[pos].getLength();

		if(yp<0) yp=0;
		if(yp+quadro[pos].getHeight()>=DEFSIZEY) yp=DEFSIZEY-quadro[pos].getHeight();



for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			if(blocked == false)
				if(this->quadro[pos].blocos[i][j].getStatus() != FREE) 
				{
					if(this->quadro[pos].getTabuleiro()->PosicaoOcupada(xp+i, yp+j) == true)
						blocked = true;
				}
	

if(blocked) 
{
	place(atual->getX(), atual->getY());
	return false;
}



		this->atual = &this->quadro[pos];

		actual_facing = pos;


		for(int g=0; g<SIZEX; g++)
			for(int j=0; j<SIZEY; j++)
			{
				if(atual->blocos[g][j].getStatus() != FREE)
				{
				if(atual->blocos[g][j].getX()>DEFSIZEX) 
					{
					place(atual->getX()-1, atual->getY());
					return true;
			
					}
				
				if(atual->blocos[g][j].getX()<0)
					{
					place(atual->getX()+1, atual->getY());
					return true;
					}
				
				if(atual->blocos[g][j].getY()<0)
					{
					place(atual->getX(), atual->getY()+1);
					return true;
					}
				
				if(atual->blocos[g][j].getY()>DEFSIZEY)
					{

					place(atual->getX(), atual->getY()-3);
					return true;
					}
								
				}
			}




			//atual->place(atual->getX(), atual->getY());
		
			place(atual->getX(), atual->getY());

			return true;
}



