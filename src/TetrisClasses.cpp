#include "TetrisClasses.h"


// classe TetrisBlock
//
//////////////////////////////////////////////////////////////////////


TetrisBlock::TetrisBlock(CDXScreen *Tela, TetrisPlayField * tabul, int piece_num)
{


tela = Tela;

x_tab = tabul->getX();

y_tab = tabul->getY();


bloco = new CDXSurface();


if(bloco->Create(tela, "pecas.bhg")<0)
		CDXError(NULL, "Impossível abrir figura");



if(piece_num == 666)
	this->peca = new_piece(tabul->getTabuleiro());
else
	this->peca = new_piece(tabul->getTabuleiro(), piece_num);

}







TetrisBlock::~TetrisBlock()
{


	SAFEDELETE(bloco);

	tela = NULL;

	peca->~framedblock();


}




void TetrisBlock::place(int x, int y)
{

	this->peca->place(x,y);
	

}


void TetrisBlock::setTabuleiro(TetrisPlayField *tabul)
{
	this->peca->setTabuleiro(tabul->getTabuleiro());
	this->x_tab = tabul->getX();
	this->y_tab = tabul->getY();
}



bool TetrisBlock::rotate()
{

	return(peca->rotate());
	
}


bool TetrisBlock::moveLeft()
{

	return peca->move(peca->getX()-1, peca->getY());

}


bool TetrisBlock::moveUp()
{

	return peca->move(peca->getX(), peca->getY()-1);

}


bool TetrisBlock::moveRight()
{

		return peca->move(peca->getX()+1, peca->getY());

}


bool TetrisBlock::moveDown()
{

		return peca->move(peca->getX(), peca->getY()+1);
}


void TetrisBlock::RenderAt(int x, int y, float scale)
{
	RECT ret = {0,0,XSPACING,YSPACING};


	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			if(peca->atual->blocos[i][j].getStatus() != FREE)
			{
			ret.left = (peca->atual->blocos[i][j].getStatus()-1)*XSPACING+1;
			ret.right =((peca->atual->blocos[i][j].getStatus()-1)*XSPACING)+XSPACING;
			bloco->DrawBlkScaled(tela->GetBack(), scale*XSPACING*i+x, scale*YSPACING*j+y, &ret, scale);

			}


}

void TetrisBlock::Render()
{
	
	int posX = peca->getX();
	int posY = peca->getY();

	RECT ret = {0,0,XSPACING,YSPACING};


	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			if(peca->atual->blocos[i][j].getStatus() != FREE)
			{
			ret.left = (peca->atual->blocos[i][j].getStatus()-1)*XSPACING+1;
			ret.right =((peca->atual->blocos[i][j].getStatus()-1)*XSPACING)+XSPACING;
			bloco->DrawBlk(tela->GetBack(), x_tab+(XSPACING*i)+(XSPACING*posX), y_tab+(YSPACING*j)+(YSPACING*posY), &ret);

			}
	
}





// TetrisPlayField: implementacao
//
//////////////////////////////////////////////////////////////////////


TetrisPlayField::TetrisPlayField(CDXScreen * Tela, int xi, int yi)
{

	Xinic = xi;
	Yinic = yi;


score = new CDXBitmapFont();

pecas = new CDXSurface();

TabDrawing = NULL;

this->tela = Tela;


if(pecas->Create(Tela, "pecas.bhg")<0)
		CDXError(NULL, "Impossível abrir figuras");


score->CreateFromFile(Tela, "font.bhf", 32, 32, ' ', 60);



tabu = new Tetristab();


}


void TetrisPlayField::setBack(char * filename)
{

TabDrawing = new CDXSurface();

if(TabDrawing->Create(tela, filename)<0)
		CDXError(NULL, "Impossível abrir Tabuleiro");

}


TetrisPlayField::~TetrisPlayField()
{

	SAFEDELETE(TabDrawing);

	SAFEDELETE(score);
	
	SAFEDELETE(pecas);

}



Tetristab * TetrisPlayField::getTabuleiro()
{ 
	return tabu;
}


int TetrisPlayField::getX()
{

return Xinic;
}


int TetrisPlayField::getY()
{

return Yinic;
}



void TetrisPlayField::DisplayTotalElims(int x, int y)
{


char buffer[10];

_itoa(tabu->getTotalElim(), buffer, 10);


score->DrawTrans(x,y, buffer, tela->GetBack());



}


void TetrisPlayField::Draw()
{

	if(TabDrawing)
		TabDrawing->DrawBlk(tela->GetBack(), getX(), getY());

	RECT ret = {0,0,XSPACING,YSPACING-1};


	for(int i=0; i<DEFSIZEX; i++)
		for(int j=0; j<DEFSIZEY; j++)
			if(tabu->tab[i][j] != FREE)
			{
				ret.left = (tabu->tab[i][j]-1)*XSPACING+1;
				ret.right =((tabu->tab[i][j]-1)*XSPACING)+XSPACING;
				pecas->DrawBlk(tela->GetBack(), getX()+(XSPACING*i), getY()+(YSPACING*j), &ret);
			}
				



	

}



