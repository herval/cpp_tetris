// hERV.Tetris versao 0.5a
// Ultima atualizacao: 10/03/2001, 13:00 PM
//
// programa principal



#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <windowsx.h>
#include <stdio.h>


#include "TetrisClasses.h"





#define GAMEOVER -1
#define BEGIN 1
#define INIT 0
#define PAUSE 2



char AppName[] = "Herv.Tetris version 0.5 Alpha Class";
char Classname[] = "Herv.Tetris";

HINSTANCE hInst;
HWND hWnd;


CDXScreen * tela;

CDXSurface * fundo;

CDXScreen * aux;


CDXMusic * musica;

CDXSound * sounds;

CDXSoundBuffer * buffer;

TetrisPlayField * tabul;

TetrisPlayField * reserva;

int delay = 0;
int mus_id = 0;
int state = 0;

int neXt;
TetrisBlock * bloco_atual;
TetrisBlock * proximo_bloco;




CDXBitmapFont * Title;


/////////////////////////////////////////////////////////////////

void paused()
{
	//if(musik->IsPlaying(mus_id) == TRUE) musik->Stop(mus_id);
//	musica->Pause();
	

	state = PAUSE;

	fundo->DrawBlk(tela->GetBack(), 0, 0);

	
	tabul->Draw();

	tabul->DisplayTotalElims(210,365);


	proximo_bloco->RenderAt(180,108, 0.7f);

	Title->DrawTrans(230, 220, "JOGO PARADO", tela->GetBack());



	tela->Flip();

	
}

/////////////////////////////////////////////////////////////////

void game_over()
{

	musica->Stop();

	state = GAMEOVER;


	fundo->DrawBlk(tela->GetBack(), 0, 0);


	tabul->Draw();

	proximo_bloco->RenderAt(180,108, 0.7f);
	tabul->DisplayTotalElims(210,365);


	Title->DrawTrans(230, 220, "GAME OVER!!!", tela->GetBack());


	tela->Flip();


	buffer->Play(1);


}

//////////////////////////////////////////////



void lancamento_novo()
{




	tabul->getTabuleiro()->eliminateAll();
	
	bloco_atual->~TetrisBlock();

	bloco_atual = proximo_bloco;
	
	bloco_atual->setTabuleiro(tabul);

	bloco_atual->place(3,0);
	


	
	proximo_bloco = new TetrisBlock(tela, reserva);

	proximo_bloco->place(3,0);

	neXt = proximo_bloco->peca->atual->getStatus();


  if(bloco_atual->moveDown() == false) state = GAMEOVER;


}

/////////////////////////////////////////////////////////////////


bool move_baixo()
{
	if(bloco_atual->moveDown() == false)
	{
		lancamento_novo();
		return false;
	}


	return true;
}



/////////////////////////////////////////////////////////////////

void sound_switch()
{


	
	
	musica->Play("jingle.mid");
	musica->Restart();

}



/////////////////////////////////////////////////////////////////


void drop_down()
{
	while(move_baixo());

}

/////////////////////////////////////////////////////////////////


void finaliza_CDX()
{

	SAFEDELETE(Title);

	SAFEDELETE(buffer);

	SAFEDELETE(sounds);
	
	SAFEDELETE(musica);

	
	SAFEDELETE(aux);

	SAFEDELETE(fundo);

	SAFEDELETE(tela);

}


////////////////////////////////////////////////////////////


long PASCAL WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
switch(state)
{
	case BEGIN:
	switch(message)
	{

		case WM_KEYDOWN:switch(wParam)
			            {
				            case VK_ESCAPE: // if ESC key was hit, quit program
								tela->FadeToBlack(2000);
								finaliza_CDX();
								PostQuitMessage(0);
								break;
 				            break;
							case VK_LEFT: bloco_atual->moveLeft(); break;
							case VK_RIGHT: bloco_atual->moveRight(); break;
							case VK_UP: bloco_atual->rotate(); break;
							case VK_DOWN: move_baixo();
										break;
							case VK_PAUSE: paused(); break;
							case VK_SPACE: drop_down(); break;
							case VK_F1: sound_switch(); break;
				          }
		                break;

		case WM_DESTROY:    
			                PostQuitMessage(0); // terminate the program
		                    break;
		case WM_TIMER: move_baixo();
					break;
		
		
	}
	break;
	

	case PAUSE: switch(message)
				{
				case WM_KEYDOWN: if(wParam==VK_PAUSE) 
					{
					sound_switch();
					state = BEGIN;
					break;
					}
				}
	break;
	
	
	case GAMEOVER: switch(message)
				   {
					case WM_KEYDOWN: if(wParam == VK_ESCAPE)
									 {
										tela->FadeToBlack(2000);
										finaliza_CDX();
										PostQuitMessage(0);
									 }
									break;
				   }
	break;

}
	return DefWindowProc(hWnd, message, wParam, lParam);
}



/////////////////////////////////////////////////////////////////



BOOL Inicia_CDX()
{


	

	tela = new CDXScreen();
	if(tela==NULL)
		CDXError(NULL, "Imposs�vel criar CDXScreen");


	if(tela->CheckIfVideoModeExists(800,600,16) == TRUE)
	{
//		if(tela->CreateWindowed(hWnd, 800, 600) != FALSE)
		if(tela->CreateFullScreen(hWnd, 800, 600, 16) != FALSE)
			CDXError(NULL, "Imposs�vel inicializar v�deo");
	}

	
	if(tela->LoadPalette("tabuleiro.bhg")<0)
		CDXError(NULL, "Imposs�vel abrir paleta");


	
	fundo = new CDXSurface();

		if(fundo->Create(tela, "tabuleiro.bhg") < 0)
			CDXError(NULL, "Impossivel abrir tabuleiro");




	tabul = new TetrisPlayField(tela, 459, 7);
	
	reserva = new TetrisPlayField(tela, 119, 7);


	
	bloco_atual = new TetrisBlock(tela, tabul);
	bloco_atual->place(3,0);
	
	proximo_bloco = new TetrisBlock(tela, reserva);
	proximo_bloco->place(3,0);

	neXt = proximo_bloco->peca->atual->getStatus();

	



	musica = new CDXMusic(hWnd);



	sounds = new CDXSound();
	if(sounds->Create(hWnd) != DS_OK)
		CDXError(NULL, "Erro ao inicializar DirectSound");

	
	buffer = new CDXSoundBuffer();

	buffer->Load(sounds, "over.snd");


	

	Title = new CDXBitmapFont();
		Title->CreateFromFile(tela, "font.bhf", 32, 32, ' ', 60);


return TRUE;

}

/////////////////////////////////////////////////////////////////


static void Ajusta_janela()
{
	RECT rect = {0, 0, 800, 600};
	DWORD dwStyle;

	dwStyle = GetWindowStyle(hWnd);
	dwStyle &= ~WS_POPUP;
	dwStyle |= WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU;

	SetWindowLong(hWnd, GWL_STYLE, dwStyle);


	AdjustWindowRectEx(&rect, GetWindowStyle(hWnd), GetMenu(hWnd) != NULL, GetWindowExStyle(hWnd));

	SetWindowPos(hWnd, NULL, 0, 0, rect.right-rect.left, rect.bottom-rect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

	SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);

}



/////////////////////////////////////////////////////////////////


void roda_cena()
{


	fundo->DrawBlk(tela->GetBack(), 0, 0);
	

	tabul->Draw();


	proximo_bloco->RenderAt(180,108, 0.7f);


	tabul->DisplayTotalElims(210,365);
	

	tela->Flip();

	tela->GetBack()->Fill(0);
	

}


/////////////////////////////////////////////////////////////////


BOOL Inicia(int nCmdShow)
{

	WNDCLASS WndClass;

	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = WinProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInst;
	WndClass.hIcon = LoadIcon(0, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(0, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.lpszMenuName = AppName;
	WndClass.lpszClassName = Classname;
	RegisterClass(&WndClass);

    // create a window which covers the whole screen
    // this is needed for fullscreen CDX apps
	hWnd = CreateWindowEx(
		WS_EX_TOPMOST,
		Classname,
		AppName,
		WS_POPUP,
		0,0,
		GetSystemMetrics(SM_CXFULLSCREEN),
		GetSystemMetrics(SM_CYFULLSCREEN),
		NULL,
		NULL,
		hInst,
		NULL);

    // when hWnd = -1 there was an error creating the main window
    // CDXError needs a CDXScreen object, if there is none at this early
    // program stage, pass it NULL
	if(!hWnd) 
        CDXError( NULL , "could not create the main window" );


    //Ajusta_janela();
	// show the main window
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);



	SetTimer(hWnd, 1, 800, NULL);

	state = PAUSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////


int PASCAL WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	
	MSG msg;

	if(!Inicia(nCmdShow))
        CDXError( NULL , "could not initialize CDX application" );

	if(!Inicia_CDX())
	{
	PostQuitMessage(0);
	return FALSE;
	}



	while(1)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if(!GetMessage(&msg, NULL, 0, 0 )) return msg.wParam;
			TranslateMessage(&msg); 
			DispatchMessage(&msg);
		}
			
		else {

			if(state == BEGIN)
			{
				roda_cena();

			}

			if(state == PAUSE)
			{
				paused();
				
			}

			if(state == GAMEOVER)
			{
				game_over();
				
			}

			}
	}
}
