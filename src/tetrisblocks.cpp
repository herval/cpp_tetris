#include "tetrisblocks.h"
// Lblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Lblock::Lblock()
{
	this->quadro[0].setPiece(0,0);
	this->quadro[0].setPiece(0,1);
	this->quadro[0].setPiece(0,2);
	this->quadro[0].setPiece(1,2);
	this->quadro[0].setStatus(AZUL);			

	this->quadro[1].setPiece(0,0);
	this->quadro[1].setPiece(1,0);
	this->quadro[1].setPiece(2,0);
	this->quadro[1].setPiece(0,1);
	this->quadro[1].setStatus(AZUL);			

	this->quadro[2].setPiece(0,0);
	this->quadro[2].setPiece(1,0);
	this->quadro[2].setPiece(1,1);
	this->quadro[2].setPiece(1,2);
	this->quadro[2].setStatus(AZUL);			

	this->quadro[3].setPiece(2,0);
	this->quadro[3].setPiece(0,1);
	this->quadro[3].setPiece(1,1);
	this->quadro[3].setPiece(2,1);
	this->quadro[3].setStatus(AZUL);			

	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


	


}


Lblock::~Lblock()
{


}



// Iblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Iblock::Iblock()
{
	this->quadro[0].setPiece(0,0);
	this->quadro[0].setPiece(0,1);
	this->quadro[0].setPiece(0,2);
	this->quadro[0].setPiece(0,3);
	this->quadro[0].setStatus(VERMELHO);		

	this->quadro[1].setPiece(0,0);
	this->quadro[1].setPiece(1,0);
	this->quadro[1].setPiece(2,0);
	this->quadro[1].setPiece(3,0);
	this->quadro[1].setStatus(VERMELHO);

	this->quadro[2].setPiece(0,0);
	this->quadro[2].setPiece(0,1);
	this->quadro[2].setPiece(0,2);
	this->quadro[2].setPiece(0,3);
	this->quadro[2].setStatus(VERMELHO);

	this->quadro[3].setPiece(0,0);
	this->quadro[3].setPiece(1,0);
	this->quadro[3].setPiece(2,0);
	this->quadro[3].setPiece(3,0);
	this->quadro[3].setStatus(VERMELHO);


	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


	


}


Iblock::~Iblock()
{


}




// Oblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Oblock::Oblock()
{
	this->quadro[0].setPiece(0,0);
	this->quadro[0].setPiece(0,1);
	this->quadro[0].setPiece(1,0);
	this->quadro[0].setPiece(1,1);
	this->quadro[0].setStatus(AMARELO);		

	this->quadro[1].setPiece(0,0);
	this->quadro[1].setPiece(0,1);
	this->quadro[1].setPiece(1,0);
	this->quadro[1].setPiece(1,1);
	this->quadro[1].setStatus(AMARELO);		

	this->quadro[2].setPiece(0,0);
	this->quadro[2].setPiece(0,1);
	this->quadro[2].setPiece(1,0);
	this->quadro[2].setPiece(1,1);
	this->quadro[2].setStatus(AMARELO);		


	this->quadro[3].setPiece(0,0);
	this->quadro[3].setPiece(0,1);
	this->quadro[3].setPiece(1,0);
	this->quadro[3].setPiece(1,1);
	this->quadro[3].setStatus(AMARELO);		

	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


	
}


Oblock::~Oblock()
{


}



// Tblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Tblock::Tblock()
{
	this->quadro[0].setPiece(1,0);
	this->quadro[0].setPiece(0,1);
	this->quadro[0].setPiece(1,1);
	this->quadro[0].setPiece(2,1);
	this->quadro[0].setStatus(AMARELO);		

	this->quadro[1].setPiece(0,0);
	this->quadro[1].setPiece(1,1);
	this->quadro[1].setPiece(0,1);
	this->quadro[1].setPiece(0,2);
	this->quadro[1].setStatus(AMARELO);		

	this->quadro[2].setPiece(0,0);
	this->quadro[2].setPiece(1,0);
	this->quadro[2].setPiece(2,0);
	this->quadro[2].setPiece(1,1);
	this->quadro[2].setStatus(AMARELO);		


	this->quadro[3].setPiece(1,0);
	this->quadro[3].setPiece(0,1);
	this->quadro[3].setPiece(1,1);
	this->quadro[3].setPiece(1,2);
	this->quadro[3].setStatus(AMARELO);		

	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


}


Tblock::~Tblock()
{


}


// Sblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Sblock::Sblock()
{
	this->quadro[0].setPiece(1,0);
	this->quadro[0].setPiece(2,0);
	this->quadro[0].setPiece(0,1);
	this->quadro[0].setPiece(1,1);
	this->quadro[0].setStatus(VERDE);

	this->quadro[1].setPiece(0,0);
	this->quadro[1].setPiece(0,1);
	this->quadro[1].setPiece(1,1);
	this->quadro[1].setPiece(1,2);
	this->quadro[1].setStatus(VERDE);		

	this->quadro[2].setPiece(1,0);
	this->quadro[2].setPiece(2,0);
	this->quadro[2].setPiece(0,1);
	this->quadro[2].setPiece(1,1);
	this->quadro[2].setStatus(VERDE);		


	this->quadro[3].setPiece(0,0);
	this->quadro[3].setPiece(0,1);
	this->quadro[3].setPiece(1,1);
	this->quadro[3].setPiece(1,2);
	this->quadro[3].setStatus(VERDE);		

 	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


}


Sblock::~Sblock()
{


}





// Zblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Zblock::Zblock()
{
	this->quadro[0].setPiece(0,0);
	this->quadro[0].setPiece(1,0);
	this->quadro[0].setPiece(1,1);
	this->quadro[0].setPiece(2,1);
	this->quadro[0].setStatus(VERMELHO);

	this->quadro[1].setPiece(1,0);
	this->quadro[1].setPiece(0,1);
	this->quadro[1].setPiece(1,1);
	this->quadro[1].setPiece(0,2);
	this->quadro[1].setStatus(VERMELHO);		

	this->quadro[2].setPiece(0,0);
	this->quadro[2].setPiece(1,0);
	this->quadro[2].setPiece(1,1);
	this->quadro[2].setPiece(2,1);
	this->quadro[2].setStatus(VERMELHO);		


	this->quadro[3].setPiece(1,0);
	this->quadro[3].setPiece(0,1);
	this->quadro[3].setPiece(1,1);
	this->quadro[3].setPiece(0,2);
	this->quadro[3].setStatus(VERMELHO);		

	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


}


Zblock::~Zblock()
{


}





// Liblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Liblock::Liblock()
{
	this->quadro[0].setPiece(1,0);
	this->quadro[0].setPiece(1,1);
	this->quadro[0].setPiece(1,2);
	this->quadro[0].setPiece(0,2);
	this->quadro[0].setStatus(AZUL);

	this->quadro[1].setPiece(0,0);
	this->quadro[1].setPiece(0,1);
	this->quadro[1].setPiece(1,1);
	this->quadro[1].setPiece(2,1);
	this->quadro[1].setStatus(AZUL);		

	this->quadro[2].setPiece(0,0);
	this->quadro[2].setPiece(1,0);
	this->quadro[2].setPiece(0,1);
	this->quadro[2].setPiece(0,2);
	this->quadro[2].setStatus(AZUL);


	this->quadro[3].setPiece(0,0);
	this->quadro[3].setPiece(1,0);
	this->quadro[3].setPiece(2,0);
	this->quadro[3].setPiece(2,1);
	this->quadro[3].setStatus(AZUL);		

	for(int i=0; i<FRAMES; i++)
		this->quadro[i].setSize();


}


Liblock::~Liblock()
{


}










// Tetristab.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////


Tetristab::Tetristab()
{
this->elim = 0;

this->setCircular(false);

this->init_tab();

}


Tetristab::~Tetristab()
{


}


void Tetristab::moveAllDown(int base_line) // 'compacta' todas as pecas tomando
{								// base_line como a linha de base
	
	for(int i=0; i<DEFSIZEX; i++)
		for(int j=base_line; j>0; j--)
		
//		if(this->tab[i][j] != FREE)
//			if(this->PosicaoOcupada(i, j-1) == true)
			{
				tab[i][j] = tab[i][j-1];
			}
}



void Tetristab::EliminateLine(int line)
{

for(int x=0; x<DEFSIZEX; x++)
	this->tab[x][line] = FREE;

}


bool Tetristab::LineEmpty(int line)
{
for(int x=0; x<DEFSIZEX; x++)
	if(this->tab[x][line] != FREE) return false;


	return true;
}

void Tetristab::eliminateAll()
{


for(int i=0; i<DEFSIZEY; i++)
	if(LineComplete(i) == true) 
	{
	EliminateLine(i);
	elim++;
	moveAllDown(i);
	}

for(i=DEFSIZEY-1; i>0; i--)
	if(LineComplete(i) == true) 
	{
	EliminateLine(i);
	elim++;
	moveAllDown(i);
	}



}

int Tetristab::getTotalElim()
{
	return this->elim;
}


int Tetristab::getTotalLines()
{
	int temp = 0;

	for(int x = 0; x<DEFSIZEY; x++)
		if( LineEmpty(x) == false ) temp ++;


		return temp;
}


bool Tetristab::LineComplete(int line)
{
bool retorno = true;
	
	
	for(int x=0; x<DEFSIZEX; x++)
		if(this->tab[x][line] == FREE) retorno = false;

	return retorno;
}






///////// FUNCOES EXTRAS /////////////////////


///////// FUNCOES EXTRAS /////////////////////


framedblock * new_piece(tabuleiro *tab)
{

	framedblock *temp;
	srand( (unsigned)time( NULL ) );
	
	int Piece = 100;

	while(Piece > 7)
		Piece = rand();

	

	switch(Piece)
	{
	case 0: temp = new Lblock(); break;
	case 1: temp = new Iblock(); break;
	case 2: temp = new Oblock(); break;
	case 3: temp = new Liblock(); break;
	case 4: temp = new Sblock(); break;
	case 5: temp = new Zblock(); break;
	case 6: temp = new Tblock(); break;
	default: temp = new Lblock(); break;
	}


	temp->setTabuleiro(tab);

	temp->place(3,0);
	
	return temp;


}




framedblock * new_piece(tabuleiro *tab, int num_piece)
{

	framedblock *temp;

	switch(num_piece)
	{
	case 0: temp = new Lblock(); break;
	case 1: temp = new Iblock(); break;
	case 2: temp = new Oblock(); break;
	case 3: temp = new Liblock(); break;
	case 4: temp = new Sblock(); break;
	case 5: temp = new Zblock(); break;
	case 6: temp = new Tblock(); break;
	default: temp = new Sblock(); break;
	}


	temp->setTabuleiro(tab);

	temp->place(3,0);
	
	return temp;

}
