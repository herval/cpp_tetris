// Biblioteca TetrisBlocks: blocos e tabuleiro especificos para o jogo TETRIS
// Programador: Herval Freire de A. J�nior
//
// Ultima versao: 6/03/2001, 21:43PM



#include "blocks.h"

#define OK 1
#define AMARELO 1
#define AZUL 2
#define VERDE 3
#define VERMELHO 4

// Lblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (LBLOCK)
#define LBLOCK


class Lblock: public framedblock
{
public:
	Lblock();
	virtual ~Lblock();

};

#endif




// Iblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (IBLOCK)
#define IBLOCK


class Iblock: public framedblock
{
public:
	Iblock();
	virtual ~Iblock();

};

#endif



// Oblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (OBLOCK)
#define OBLOCK


class Oblock: public framedblock
{
public:
	Oblock();
	virtual ~Oblock();

};

#endif



// Tblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (TBLOCK)
#define TBLOCK


class Tblock: public framedblock
{
public:
	Tblock();
	virtual ~Tblock();

};

#endif




// Sblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (SBLOCK)
#define SBLOCK


class Sblock: public framedblock
{
public:
	Sblock();
	virtual ~Sblock();

};

#endif



// Zblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (ZBLOCK)
#define ZBLOCK


class Zblock: public framedblock
{
public:
	Zblock();
	virtual ~Zblock();

};

#endif



// Liblock: subtipo de framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (LIBLOCK)
#define LIBLOCK


class Liblock: public framedblock
{
public:
	Liblock();
	virtual ~Liblock();

};

#endif





// classe Tetristab: tabuleiro especifico do jogo tetris
//
//////////////////////////////////////////////////////////////////////


#if !defined (TETRISTAB)
#define TETRISTAB


class Tetristab: public tabuleiro
{
public:
	Tetristab();
	virtual ~Tetristab();
	bool LineComplete(int line);
	void EliminateLine(int line);
	void moveAllDown(int base_line);
	bool LineEmpty(int line);
	int getTotalLines();
	int getTotalElim();
	void eliminateAll();

private:
	int elim;


};

#endif


///////// FUNCOES EXTRAS /////////////////////


framedblock * new_piece(tabuleiro *tab);
framedblock * new_piece(tabuleiro *tab, int num_piece);
