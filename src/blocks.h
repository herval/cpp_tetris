// Blocks v. 0.5 Alpha Class
// Programador: Herval Freire de A. J�nior
//
// Versao experimental
// classes genericas de controle de blocos e tabuleiros bidimensionais
//
// Ultima versao: 6/03/2001, 21:43PM


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <iostream.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <math.h>
#include <time.h>
#include <iostream.h>


using namespace std;


#define FRAMES 4 // numero de frames de um framedblock
#define DEFSIZEX 10 // tamanho do tabuleiro (x, y)
#define DEFSIZEY 18
#define FREE 0		// status dos 'pieces'
#define OK 1
#define SIZEX 4 // tamanho das pecas
#define SIZEY 4



using namespace std;


class tabuleiro;



// Classe piece
//
//////////////////////////////////////////////////////////////////////

#if !defined (PIECE)
#define PIECE


class piece
{
public:
	piece();
	bool move(int length);
	virtual ~piece();
	void setStatus(int x);
	int getStatus();
	void setTabuleiro(tabuleiro *tab); // seta o tabuleiro da peca
	tabuleiro * getTabuleiro(); // retorna o tabuleiro da peca
	void place(int x, int y); //coloca peca no tabuleiro
	int getIcon();
	void setIcon(int x);
	bool move(int quantX, int quantY);
	int getX();
	int getY();
	void setX(int x);
	void setY(int y);
	void setFacing(float ang);
	float getFacing();


private:
	tabuleiro * view;
	int status;
	int situation;
	int x, y;
	float facing;
	int icon; // um representativo qualquer... cor, talvez??

};

#endif





// Classe tabuleiro
//
//////////////////////////////////////////////////////////////////////

#if !defined (TABULEIRO)
#define TABULEIRO

class tabuleiro  
{
public:

	void removeAt(int x, int y);
	bool PosicaoOcupada(int x, int y);
	tabuleiro(int x = DEFSIZEX, int y = DEFSIZEY);
	virtual ~tabuleiro();
	void draw(int xinic = 0, int yinic = 0);
	void place(piece *pec);
	void init_tab();
	void remove(piece *pec);
	void setCircular(bool is);
	bool isCircular();
	int tab[DEFSIZEX][DEFSIZEY];

protected:
	int sizex, sizey;
	vector<piece *> eixo; // todas as pecas 'renderable'
	
	bool circular;

	//piece * eixo[DEFSIZEX][DEFSIZEY];

};


#endif

// classe block
//
//////////////////////////////////////////////////////////////////////


#if !defined (BLOCK)
#define BLOCK

class block
{
public:
	tabuleiro * getTabuleiro();
	int getLength();
	void setPos(int x, int y);
	void setIcon(int x);
	void setStatus(int x);
	int getStatus(); // status da peca (armazenado em SITUATION...)
	int getHeight();
	int getX();
	int getY();
	void setSize();
	bool move(int qx, int qy);
	void setTabuleiro(tabuleiro *t);
	block();
	virtual ~block();
	void setPiece(int posx, int posy);
	void unsetPiece(int posx, int posy);
	void place(int x, int y);
	void remove();

	piece blocos[SIZEX][SIZEY];	

private:

	int situation;
	int xpos, ypos;
	int comprimento;
	int altura;

};

#endif


// classe framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (FRAMEDBLOCK)
#define FRAMEDBLOCK


class framedblock
{
public:
	framedblock();
	virtual ~framedblock();

	int getX();
	int getY();
	bool move(int x, int y);
	void setTabuleiro(tabuleiro *tab);
	void place(int x, int y);
	bool rotate();
	block * atual;
	

protected:
	int actual_facing;
	block quadro[FRAMES];


};

#endif


