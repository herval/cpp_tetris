// TETRISCLASSES - classes genericas para o jogo Tetris

// Programador: Herval Freire de A. J�nior
//
// uso das classes Block como base e tabuleiro, pecas descritas no tetrisblocks.h
// Usando o CDX 3.0 para acesso aos recursos DirectX
//
// versao 0.5a
// ultima alteracao: 09/03/2001, 15:50 PM


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define CDXINCLUDEALL
#include <cdx.h>

#include "blocks.h"
#include "tetrisblocks.h"
#include "addons/CDXBitmapFont.h"





#define XSPACING 32
#define YSPACING 32




// classe TetrisPlayField: tabuleiro grafico de mapeamento do Tetristab
//
//////////////////////////////////////////////////////////////////////

#if !defined(TETRISPLAYFIELD)
#define TETRISPLAYFIELD



class TetrisPlayField
{
public:
	TetrisPlayField(CDXScreen * tela, int xi, int yi); // recebe o CDXScreen onde pode escrever
	virtual ~TetrisPlayField();
	void setBack(char *filename);
	Tetristab * getTabuleiro();
	int getX();
	int getY();
	void Draw();
	void DisplayTotalElims(int x, int y);


private:
	Tetristab *tabu;

	CDXSurface * pecas;
	CDXBitmapFont * score;
	CDXScreen * tela;
	CDXSurface * TabDrawing;
	int Xinic;
	int Yinic;


};

#endif



// classe TetrisBlock: bloco generico
//
//////////////////////////////////////////////////////////////////////

#if !defined(TETRISBLOCK)
#define TETRISBLOCK


class TetrisBlock
{
public:
	void place(int x, int y);
	bool moveDown();
	bool moveUp();
	bool moveRight();
	bool moveLeft();
	TetrisBlock(CDXScreen * tela, TetrisPlayField * tabul, int piece_num = 666);
	void setTabuleiro(TetrisPlayField *tabul);
	virtual ~TetrisBlock();
	bool rotate();
	void RenderAt(int x, int y, float scale);
	int actual_facing;
	void Render();

	
	framedblock * peca;

	
	

protected:
	CDXSurface * bloco;
	CDXScreen *tela;

	int x_tab;
	int y_tab;
};

#endif

