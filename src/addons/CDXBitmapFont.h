//////////////////////////////////////////////////////////////////////////////////
// Project Name:    [ CDX BitmapFont ]
// Original Author: [ Ioannis Karagiorgos - karagior@pluto.fernuni-hagen.de ]
// Author:          [ Jimb Esser - wasteland@writeme.com ]
// Date:            [ 7.4.1999 ]
// Revision:        [ 2.00 ]
//  Updated to work with CDX 3.0 and other bug fixes by Jimb Esser
//  Note: the draw clipped functions have not yet been updated
//////////////////////////////////////////////////////////////////////////////////


// Note on CreateFromFile:
// A good source of graphical fonts is: http://cgi.algonet.se/htbin/cgiwrap?user=guld1&script=fonts.pl
//   but, these all need to be massaged into a readable format, specifically
//   8-bit BMP (Photoshop may try to save as 4-bit, but CDX doesn't
//   do 4-bit files).


#ifndef __CDX_BITMAPFONT__
#define __CDX_BITMAPFONT__

#define CDXINCLUDEALL
#include <cdx.h>


#ifndef CDX_CENTERJUSTIFY
#define CDX_CENTERJUSTIFY 1
#endif
#ifndef CDX_RIGHTJUSTIFY
#define CDX_RIGHTJUSTIFY 3
#endif

// Function used internally that makes a DWORD color value from a RGB triplet
void MakeColor(CDXScreen* Screen, DWORD &c, long r, long g, long b);


class CDXBitmapFont
{
public:

	typedef struct 
	{
		RECT    Rectangle;
		int		Width , Height;
	} CDXBITMAPFONT;

	CDXBITMAPFONT	CDXBitmapFontArray[ 256 ];

	int				FontColor;
	int				FontBackgroundColor;
	int				FontColorKey;
	int				FontHeight;
    int             FontAttributes;
	char			* FontName;
    int             TextSurfaceType;
	int				m_iFirstChar; // The lowest character that can be displayed

	CDXSurface *	TextSurface;
   	CDXScreen  *	Screen;

    CDXBitmapFont();
	~CDXBitmapFont();

	HRESULT Create( CDXScreen * pScreen);
	// Note on ColorKey:
	//		if your screen is in 8bit mode, then this parameter must be
	//		the index of the color (dependant on palette), for any other
	//		bitdepth, pass in an RGB(r,g,b) macro parameter
	HRESULT Create( CDXScreen * pScreen, char * FontNam , int Height , int Color = RGB( 255 , 255 , 255 ), int ColKey = RGB(0,0,0), 
			int BackColor = RGB(0,0,0), int Attributes = FW_NORMAL , BYTE memoryType= CDXMEM_VIDTHENSYS );

	// FirstChar is the first character in the file (ascii value)
	// NumChars is the number of characters in the file.  This shouldn't be more than 256 - FirstChar
	//   ' ' and 60 chars seem common in the font files I've found.
	HRESULT CreateFromFile( CDXScreen * pScreen, const char * szFileName, int Width, int Height,
			char FirstChar = ' ', int NumChars = 60, BYTE memoryType= CDXMEM_VIDTHENSYS);



    void PaintCharactersInSurface( void );
    void PaintCharactersInSurface2( void ); // Prototype of an antialiased version.  Not currently used

	HRESULT Draw(int X, int Y, char * Text , CDXSurface* lpDDest , int Length = -1 );
//    HRESULT DrawClipped(int X, int Y, char * Text , CDXSurface* lpDDest, LPRECT ClipRect , int Length = -1);
    HRESULT DrawTrans(int X, int Y, char * Text , CDXSurface* lpDDest , int Length = -1);
//    HRESULT DrawTransClipped(int X, int Y, char * Text , CDXSurface* lpDDest, LPRECT ClipRect , int Length = -1);

	// iWidth is the width of the box in which the text should be aligned
    HRESULT DrawAligned( int X , int Y , int Width , char * Text , CDXSurface * lpDDest , int Align );
    HRESULT DrawAlignedTrans( int X , int Y , int Width , char * Text , CDXSurface * lpDDest , int Align );

    void SetFont( char * FontNam , int Height , int Color , int Attributes = FW_NORMAL );
    void SetColor( int Color );
    void SetColorKey( int ColorKey );
    void SetBackgroundColor( int Color );

    int GetCharacterWidth( char ch );
    int GetCharacterHeight( char ch );
    int GetTextWidth( char * Text );

    void Restore( );
};

#endif